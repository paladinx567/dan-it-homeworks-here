const { src, dest, watch, parallel, series } = require('gulp');

const scss          = require('gulp-sass');
const concat        = require('gulp-concat');
const browserSync   = require('browser-sync').create();
const uglify        = require('gulp-uglify-es').default;
const autoprefixer  = require('gulp-autoprefixer');
const imagemin      = require('gulp-imagemin');
const clean         = require('gulp-clean');

function browser(){
    browserSync.init({
        server : {
            baseDir: 'src/'
        }
    })
}

function cleanDist(){
    return src('dist', {allowEmpty: true})
        .pipe(clean())
}

function styles(){
    return src('src/scss/style.scss')
        .pipe(scss({outputStyle: 'compressed'}))
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 10 version'],
            grid: true
        }))
        .pipe(dest('src/css'))
        .pipe(browserSync.stream())
}

function scripts(){
    return src('src/js/main.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('src/js'))
        .pipe(browserSync.stream())
}

function images(){
    return src('src/img/**/*')
        .pipe(imagemin([
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(dest('dist/img'))
}

function watching(){
    watch(['src/scss/**/*.scss'], styles).on('change', browserSync.reload);
    watch(['src/js/**/*.js', '!src/js/scripts.min.js'], scripts).on('change', browserSync.reload);
    watch(['src/*.html']).on('change', browserSync.reload)
}

function buildDist(){
    return src([
        'src/css/style.min.css',
        'src/fonts/**/*',
        'src/js/scripts.min.js',
        'src/*.html'
    ], {base: 'src'})
        .pipe(dest('dist'))
}

exports.styles = styles;
exports.watching = watching;
exports.browser = browser;
exports.scripts = scripts;
exports.images = images;
exports.cleanDist = cleanDist;

exports.build = series(cleanDist, images, buildDist);

exports.dev = parallel(scripts, browser, watching);