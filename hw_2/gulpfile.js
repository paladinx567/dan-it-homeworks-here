const gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    clean = require('gulp-clean'),
    sass = require("gulp-sass"),
    imagemin = require("gulp-imagemin");

/** PATHS **/
const paths = {
    src: {
        styles: 'src/css/**/*.css',
        html: 'src/index.html',
        image: 'src/img/**/*',
        js: 'src/js/*'
    },
    build: {
        self: 'build',
        html: 'build/',
        styles: 'build/css/',
        image: 'build/img/',
        js: 'build/js/'
    }
};

/** FUNCTIONS **/
const cleanBuild = () => (
    gulp.src(paths.build.self, {allowEmpty: true})
        .pipe(clean())
);

const scssBuild = () => (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.build.styles))
        .pipe(browserSync.stream())
);
const imgBuild = () => (
    gulp.src(paths.src.image)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.build.image))
        .pipe(browserSync.stream())
);
const htmlBuild = () => (
    gulp.src(paths.src.html)
        .pipe(gulp.dest(paths.build.html))
        .pipe(browserSync.stream())
)

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });

    gulp.watch(paths.src.styles, scssBuild).on('change', browserSync.reload);
    gulp.watch(paths.src.image, imgBuild).on('change', browserSync.reload);
    gulp.watch(paths.src.html).on('change', browserSync.reload);
};

/** TASKS **/

gulp.task('build', gulp.series(
    cleanBuild,
    htmlBuild,
    scssBuild,
    imgBuild
));

gulp.task('dev', watcher);
