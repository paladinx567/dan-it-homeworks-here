(function($) {
    $(function() {
        $("ul.tabs__nav").on("click", "li:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("div.tab__content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
        });
    });
})($);
